turtlescript.mk16.de
====================

This repository contains a copy of the [turtlescript.mk16.de](http://turtlescript.mk16.de/ "turtlescript.mk16.de") website.  
For newer files, you may not find a copy of these files here.

To clone the turtlescript.mk16.de website of bitbucket.org with git use the following command:  
```
git clone https://marek22k@bitbucket.org/marek22k/turtlescript_mk16_de.git
```

![Logo](//turtlescript.mk16.de/img/turtle.png "Logo")